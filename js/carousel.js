/**
 * Created by Andrew Skorik on 31.08.2015.
 */

'use strict';


/* ///////////////////////////////////////////////////////////////////////////////////////////////
////
//// 3D CAROUSEL TEST WORK
//// An example of implementing carousel for modern browsers
//// with css3 transformations and filters (canvas fallback for blur filter).
//// We can use only native js by conditions so it's a little boilerplate
//// (Hey guys! Allow at least Modernizr for next person :) )
//// Sorry if this not enough cleaned and tested
////
////////////////////////////////////////////////////////////////////////////////////////////// */



(function () {

    /* ///////////////////////////////////////////////////////////////////////////////////////////////
     // service methods
     ////////////////////////////////////////////////////////////////////////////////////////////// */

    // console lack handling
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }


    var decorate = function (object, traits) {
        for (var accessor in traits) object[accessor] = traits[accessor];

        return object;
    };

    var declare = function (qualifiedName, object, scope) {
        var nodes = qualifiedName.split('.')
            , node = scope || carousel.global
            , lastNode
            , nodeName;

        for (var i = 0, n = nodes.length; i < n; i++) {
            lastNode = node;
            nodeName = nodes[i];

            node = (null == node[nodeName] ? node[nodeName] = {} : node[nodeName]);
        }

        if (null == object)
            return node;

        return lastNode[nodeName] = object;
    };

    var $ = function (el) {
        return document.querySelectorAll(el);
    }


    var getVendorPrefix = function (arrayOfPrefixes) {

        var tmp = document.createElement("div");
        var result = "";

        for (var i = 0; i < arrayOfPrefixes.length; ++i) {
            var prefix = tmp.style[arrayOfPrefixes[i]]
            if (typeof prefix != 'undefined') {
                result = arrayOfPrefixes[i];
                break;
            } else {
                result = null;
            }
        }

        return result;
    };

    var supportsPreserve3d = function () {

        var element = document.createElement('p'),

            propertys = {
                'webkitTransformStyle': '-webkit-transform-style',
                'MozTransformStyle': '-moz-transform-style',
                'msTransformStyle': '-ms-transform-style',
                'transformStyle': 'transform-style'
            };

        document.body.insertBefore(element, null);

        for (var i in propertys) {
            if (element.style[i] !== undefined) {
                element.style[i] = "preserve-3d";
            }
        }

        var st = window.getComputedStyle(element, null),
            transform = st.getPropertyValue("-webkit-transform-style") ||
                st.getPropertyValue("-moz-transform-style") ||
                st.getPropertyValue("-ms-transform-style") ||
                st.getPropertyValue("transform-style");


        document.body.removeChild(element);

        if (transform !== 'preserve-3d') return false;

        return true;

    };


    // canvas filters helper

    var Filters = {};

    Filters.getPixels = function (img, bounds) {
        bounds = bounds || 0
        var c = this.getCanvas(img.width + bounds * 2, img.height + bounds * 2);
        var ctx = c.getContext('2d');
        ctx.drawImage(img, bounds, bounds);
        return ctx.getImageData(0, 0, c.width, c.height);
    };

    Filters.getCanvas = function (w, h) {
        var c = document.createElement('canvas');
        c.width = w;
        c.height = h;
        return c;
    };

    Filters.tmpCanvas = document.createElement('canvas');
    Filters.tmpCtx = Filters.tmpCanvas.getContext('2d');

    Filters.createImageData = function (w, h) {
        return this.tmpCtx.createImageData(w, h);
    };

    Filters.filterImage = function (filter, image, bounds, var_args) {
        var args = [this.getPixels(image, bounds)];
        for (var i = 3; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        return filter.apply(null, args);
    };

    Filters.grayscale = function (pixels, args) {
        var d = pixels.data;
        for (var i = 0; i < d.length; i += 4) {
            var r = d[i];
            var g = d[i + 1];
            var b = d[i + 2];
            // CIE luminance for the RGB
            // The human eye is bad at seeing red and blue, so we de-emphasize them.
            var v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
            d[i] = d[i + 1] = d[i + 2] = v
        }

        return pixels;
    };


    Filters.fastBlur = function (imageData, blurX, blurY, quality) {

        var MUL_TABLE = [1, 171, 205, 293, 57, 373, 79, 137, 241, 27, 391, 357, 41, 19, 283, 265, 497, 469, 443, 421, 25, 191, 365, 349, 335, 161, 155, 149, 9, 278, 269, 261, 505, 245, 475, 231, 449, 437, 213, 415, 405, 395, 193, 377, 369, 361, 353, 345, 169, 331, 325, 319, 313, 307, 301, 37, 145, 285, 281, 69, 271, 267, 263, 259, 509, 501, 493, 243, 479, 118, 465, 459, 113, 446, 55, 435, 429, 423, 209, 413, 51, 403, 199, 393, 97, 3, 379, 375, 371, 367, 363, 359, 355, 351, 347, 43, 85, 337, 333, 165, 327, 323, 5, 317, 157, 311, 77, 305, 303, 75, 297, 294, 73, 289, 287, 71, 141, 279, 277, 275, 68, 135, 67, 133, 33, 262, 260, 129, 511, 507, 503, 499, 495, 491, 61, 121, 481, 477, 237, 235, 467, 232, 115, 457, 227, 451, 7, 445, 221, 439, 218, 433, 215, 427, 425, 211, 419, 417, 207, 411, 409, 203, 202, 401, 399, 396, 197, 49, 389, 387, 385, 383, 95, 189, 47, 187, 93, 185, 23, 183, 91, 181, 45, 179, 89, 177, 11, 175, 87, 173, 345, 343, 341, 339, 337, 21, 167, 83, 331, 329, 327, 163, 81, 323, 321, 319, 159, 79, 315, 313, 39, 155, 309, 307, 153, 305, 303, 151, 75, 299, 149, 37, 295, 147, 73, 291, 145, 289, 287, 143, 285, 71, 141, 281, 35, 279, 139, 69, 275, 137, 273, 17, 271, 135, 269, 267, 133, 265, 33, 263, 131, 261, 130, 259, 129, 257, 1]
        var SHG_TABLE = [0, 9, 10, 11, 9, 12, 10, 11, 12, 9, 13, 13, 10, 9, 13, 13, 14, 14, 14, 14, 10, 13, 14, 14, 14, 13, 13, 13, 9, 14, 14, 14, 15, 14, 15, 14, 15, 15, 14, 15, 15, 15, 14, 15, 15, 15, 15, 15, 14, 15, 15, 15, 15, 15, 15, 12, 14, 15, 15, 13, 15, 15, 15, 15, 16, 16, 16, 15, 16, 14, 16, 16, 14, 16, 13, 16, 16, 16, 15, 16, 13, 16, 15, 16, 14, 9, 16, 16, 16, 16, 16, 16, 16, 16, 16, 13, 14, 16, 16, 15, 16, 16, 10, 16, 15, 16, 14, 16, 16, 14, 16, 16, 14, 16, 16, 14, 15, 16, 16, 16, 14, 15, 14, 15, 13, 16, 16, 15, 17, 17, 17, 17, 17, 17, 14, 15, 17, 17, 16, 16, 17, 16, 15, 17, 16, 17, 11, 17, 16, 17, 16, 17, 16, 17, 17, 16, 17, 17, 16, 17, 17, 16, 16, 17, 17, 17, 16, 14, 17, 17, 17, 17, 15, 16, 14, 16, 15, 16, 13, 16, 15, 16, 14, 16, 15, 16, 12, 16, 15, 16, 17, 17, 17, 17, 17, 13, 16, 15, 17, 17, 17, 16, 15, 17, 17, 17, 16, 15, 17, 17, 14, 16, 17, 17, 16, 17, 17, 16, 15, 17, 16, 14, 17, 16, 15, 17, 16, 17, 17, 16, 17, 15, 16, 17, 14, 17, 16, 15, 17, 16, 17, 13, 17, 16, 17, 17, 16, 17, 14, 17, 16, 17, 16, 17, 16, 17, 9];

        var radiusX = blurX >> 1;
        if (isNaN(radiusX) || radiusX < 0) return false;
        var radiusY = blurY >> 1;
        if (isNaN(radiusY) || radiusY < 0) return false;
        if (radiusX == 0 && radiusY == 0) return false;

        var iterations = quality;
        if (isNaN(iterations) || iterations < 1) iterations = 1;
        iterations |= 0;
        if (iterations > 3) iterations = 3;
        if (iterations < 1) iterations = 1;

        var px = imageData.data;
        var x = 0, y = 0, i = 0, p = 0, yp = 0, yi = 0, yw = 0, r = 0, g = 0, b = 0, a = 0, pr = 0, pg = 0, pb = 0, pa = 0;

        var divx = (radiusX + radiusX + 1) | 0;
        var divy = (radiusY + radiusY + 1) | 0;
        var w = imageData.width | 0;
        var h = imageData.height | 0;

        var w1 = (w - 1) | 0;
        var h1 = (h - 1) | 0;
        var rxp1 = (radiusX + 1) | 0;
        var ryp1 = (radiusY + 1) | 0;

        var ssx = {r: 0, b: 0, g: 0, a: 0};
        var sx = ssx;
        for (i = 1; i < divx; i++) {
            sx = sx.n = {r: 0, b: 0, g: 0, a: 0};
        }
        sx.n = ssx;

        var ssy = {r: 0, b: 0, g: 0, a: 0};
        var sy = ssy;
        for (i = 1; i < divy; i++) {
            sy = sy.n = {r: 0, b: 0, g: 0, a: 0};
        }
        sy.n = ssy;

        var si = null;


        var mtx = MUL_TABLE[radiusX] | 0;
        var stx = SHG_TABLE[radiusX] | 0;
        var mty = MUL_TABLE[radiusY] | 0;
        var sty = SHG_TABLE[radiusY] | 0;

        while (iterations-- > 0) {

            yw = yi = 0;
            var ms = mtx;
            var ss = stx;
            for (y = h; --y > -1;) {
                r = rxp1 * (pr = px[(yi) | 0]);
                g = rxp1 * (pg = px[(yi + 1) | 0]);
                b = rxp1 * (pb = px[(yi + 2) | 0]);
                a = rxp1 * (pa = px[(yi + 3) | 0]);

                sx = ssx;

                for (i = rxp1; --i > -1;) {
                    sx.r = pr;
                    sx.g = pg;
                    sx.b = pb;
                    sx.a = pa;
                    sx = sx.n;
                }

                for (i = 1; i < rxp1; i++) {
                    p = (yi + ((w1 < i ? w1 : i) << 2)) | 0;
                    r += ( sx.r = px[p]);
                    g += ( sx.g = px[p + 1]);
                    b += ( sx.b = px[p + 2]);
                    a += ( sx.a = px[p + 3]);

                    sx = sx.n;
                }

                si = ssx;
                for (x = 0; x < w; x++) {
                    px[yi++] = (r * ms) >>> ss;
                    px[yi++] = (g * ms) >>> ss;
                    px[yi++] = (b * ms) >>> ss;
                    px[yi++] = (a * ms) >>> ss;

                    p = ((yw + ((p = x + radiusX + 1) < w1 ? p : w1)) << 2);

                    r -= si.r - ( si.r = px[p]);
                    g -= si.g - ( si.g = px[p + 1]);
                    b -= si.b - ( si.b = px[p + 2]);
                    a -= si.a - ( si.a = px[p + 3]);

                    si = si.n;

                }
                yw += w;
            }

            ms = mty;
            ss = sty;
            for (x = 0; x < w; x++) {
                yi = (x << 2) | 0;

                r = (ryp1 * (pr = px[yi])) | 0;
                g = (ryp1 * (pg = px[(yi + 1) | 0])) | 0;
                b = (ryp1 * (pb = px[(yi + 2) | 0])) | 0;
                a = (ryp1 * (pa = px[(yi + 3) | 0])) | 0;

                sy = ssy;
                for (i = 0; i < ryp1; i++) {
                    sy.r = pr;
                    sy.g = pg;
                    sy.b = pb;
                    sy.a = pa;
                    sy = sy.n;
                }

                yp = w;

                for (i = 1; i <= radiusY; i++) {
                    yi = ( yp + x ) << 2;

                    r += ( sy.r = px[yi]);
                    g += ( sy.g = px[yi + 1]);
                    b += ( sy.b = px[yi + 2]);
                    a += ( sy.a = px[yi + 3]);

                    sy = sy.n;

                    if (i < h1) {
                        yp += w;
                    }
                }

                yi = x;
                si = ssy;
                if (iterations > 0) {
                    for (y = 0; y < h; y++) {
                        p = yi << 2;
                        px[p + 3] = pa = (a * ms) >>> ss;
                        if (pa > 0) {
                            px[p] = ((r * ms) >>> ss );
                            px[p + 1] = ((g * ms) >>> ss );
                            px[p + 2] = ((b * ms) >>> ss );
                        } else {
                            px[p] = px[p + 1] = px[p + 2] = 0
                        }

                        p = ( x + (( ( p = y + ryp1) < h1 ? p : h1 ) * w )) << 2;

                        r -= si.r - ( si.r = px[p]);
                        g -= si.g - ( si.g = px[p + 1]);
                        b -= si.b - ( si.b = px[p + 2]);
                        a -= si.a - ( si.a = px[p + 3]);

                        si = si.n;

                        yi += w;
                    }
                } else {
                    for (y = 0; y < h; y++) {
                        p = yi << 2;
                        px[p + 3] = pa = (a * ms) >>> ss;
                        if (pa > 0) {
                            pa = 255 / pa;
                            px[p] = ((r * ms) >>> ss ) * pa;
                            px[p + 1] = ((g * ms) >>> ss ) * pa;
                            px[p + 2] = ((b * ms) >>> ss ) * pa;
                        } else {
                            px[p] = px[p + 1] = px[p + 2] = 0
                        }

                        p = ( x + (( ( p = y + ryp1) < h1 ? p : h1 ) * w )) << 2;

                        r -= si.r - ( si.r = px[p]);
                        g -= si.g - ( si.g = px[p + 1]);
                        b -= si.b - ( si.b = px[p + 2]);
                        a -= si.a - ( si.a = px[p + 3]);

                        si = si.n;

                        yi += w;
                    }
                }
            }

        }

        return imageData;
    }


    // overide default functionality and add some new for IE
    // and possibly other browsers without css filters
    // implementing blur with canvas

    var ieHelper =
    {
        help: function () {

            var visProto = carousel.declarations.visualizers.CSS3Visualizer.prototype;
            var itemProto = carousel.declarations.visualizers.CSS3Item.prototype;

            itemProto.createBlurCanvas = function (w, h, bounds) {

                bounds = bounds || 0;
                var c = Filters.getCanvas(w + bounds * 2, h + bounds * 2);
                c.classList.add("layer");
                c.style.top = "-" + bounds + "px";
                c.style.left = "-" + bounds + "px";

                return c;
            };

            itemProto.addBlurCanvas = function (canvas, blurLvl) {

                var blurs = this.blurs || [];
                blurs[blurLvl] = canvas;
                this.dom.insertBefore(canvas, this.dom.firstChild);

                this.blurs = blurs;
            };

            visProto.onImageLoaded = function (item) {

                item.loadedState();

                // Create all blur levels at start. We can not load CPU with calculations on each update
                item.createBlurLevel(25, 1, 2);
                item.createBlurLevel(12, 1, 1);

                if (!item.showBg) item.dom.style.background = "transparent";

                // hide all image variants
                item.image.style.visibility = "hidden";
                item.blurs[1].style.visibility = "hidden";
                item.blurs[2].style.visibility = "hidden";

                this.invalidate();
            };

            itemProto.createBlurLevel = function (blur, quality, level) {

                var idata = Filters.filterImage(Filters.fastBlur, this.image, blur, blur, quality);

                var q = Math.pow(quality, 0.2);
                var b = blur * q + 1

                var c = this.createBlurCanvas(idata.width, idata.height, b);

                var ctx = c.getContext('2d');
                ctx.putImageData(idata, 0, 0);

                this.addBlurCanvas(c, level);

            };

            itemProto.updateEffects = function (pos, filterPrefix) {
                //TODO: improve this with calculation from items count
                var d = 2 - pos;

                // we are using class change instead of filter value change because last is very slow in Chrome
                if (Math.abs(d) == 2) {

                    //this.dom.style.boxShadow = "none";
                    if (this.image) this.image.style.visibility = "hidden";
                    if (this.blurs && this.blurs[1]) this.blurs[1].style.visibility = "hidden";
                    if (this.blurs && this.blurs[2]) this.blurs[2].style.visibility = "visible";

                } else if (Math.abs(d) == 1) {

                    //this.dom.style.boxShadow = (d > 0) ? this.leftShadow : this.rightShadow;
                    if (this.image) this.image.style.visibility = "hidden";
                    if (this.blurs && this.blurs[1]) this.blurs[1].style.visibility = "visible";
                    if (this.blurs && this.blurs[2]) this.blurs[2].style.visibility = "hidden";

                } else {

                    //this.dom.style.boxShadow = this.centerShadow;

                    if (this.image) this.image.style.visibility = "visible";
                    if (this.blurs && this.blurs[1]) this.blurs[1].style.visibility = "hidden";
                    if (this.blurs && this.blurs[2]) this.blurs[2].style.visibility = "hidden";

                }
            };
        }
    }

    /* ////////////////////////////////////////////////////////////////////////////////////////////
     // component namespace
     //////////////////////////////////////////////////////////////////////////////////////////// */

    var carousel, instance;

    carousel = {
        global: (function () {
            return window
        })(),
        getInstance: function (holder, cfg) {

            if (instance === undefined) {
                instance = new carousel.declarations.Engine();
                instance.id = "inst" + new Date().getTime();

                if (holder) instance.holder = holder;
                instance.setup(cfg);

            }
            return instance;
        }
    };

    /* ///////////////////////////////////////////////////////////////////////////////////////
     // Component core parts
     /////////////////////////////////////////////////////////////////////////////////////// */

    //TODO: Split things declarations to separate files. For easy maintaining and developement

    // CSS3 3d transforms visualizer

    var CSS3Item = {
        dom: null,
        preloadText: null,
        image: null,
        index: 0,
        position: 0,
        leftShadow: "-21px -3px 30px 0px rgba(50, 50, 50, 0.54)",
        rightShadow: "21px 3px 30px 0px rgba(50, 50, 50, 0.54)",
        centerShadow: "3px 3px 40px 0px rgba(50, 50, 50, 0.54)",
        showBg: false,

        preloadState: function () {
            this.dom.appendChild(this.preloadText);
        },

        loadedState: function () {

            this.preloadText.parentNode.removeChild(this.preloadText);
        },

        addImage: function (img) {
            this.image = img;
            this.dom.insertBefore(img, this.dom.firstChild);
            this.image.classList.add("layer");
        },

        updateEffects: function (pos, filterPrefix) {

            //TODO: improve this with calculation from items count
            var d = 2 - pos;

            // we are using class change instead of filter value change because last is very slow in Chrome
            if (Math.abs(d) == 2) {

                this.dom.style.boxShadow = "none";

                if (filterPrefix != null) {
                    this.dom.classList.remove("middleBlur");
                    this.dom.classList.remove("noBlur");
                    this.dom.classList.add("deepBlur");
                }

            } else if (Math.abs(d) == 1) {

                this.dom.style.boxShadow = (d > 0) ? this.leftShadow : this.rightShadow;

                if (filterPrefix != null) {
                    this.dom.classList.remove("noBlur");
                    this.dom.classList.remove("deepBlur");
                    this.dom.classList.add("middleBlur");
                }

            } else {
                this.dom.style.boxShadow = this.centerShadow;

                if (filterPrefix != null) {
                    this.dom.classList.remove("middleBlur");
                    this.dom.classList.remove("deepBlur");
                    this.dom.classList.add("noBlur");
                }
            }


        }
    }

    var CSS3ItemConstructor = new Function();
    decorate(CSS3ItemConstructor.prototype, CSS3Item);
    declare("declarations.visualizers.CSS3Item", CSS3ItemConstructor, carousel);

    var CSS33Visualizer = {

        transformPrefix: null,
        transitionPrefix: null,
        transformStylePrefix: null,
        filterPrefix: null,
        isSupportsPreserve3d: true,
        invalidate: null,

        //TODO: improve this for automatic calculation from items count
        positions: [
            "translateZ(-600px) translateX(-375px) rotateY(-60deg)"
            , "translateZ(-200px) translateX(-180px)rotateY(-30deg)"
            , "translateZ(200px) translateX(0px)"
            , "translateZ(-200px) translateX(180px) rotateY(30deg)"
            , "translateZ(-600px) translateX(375px)  rotateY(60deg)"
        ],

        depths: [-100, -10, 100, -10, -100],

        init: function () {

            this.transformPrefix = getVendorPrefix(["transform", "msTransform", "MozTransform", "WebkitTransform", "OTransform"]);
            this.transitionPrefix = getVendorPrefix(["transition", "msTransition", "MozTransition", "WebkitTransition", "OTransition"]);
            this.transformStylePrefix = getVendorPrefix(["transformStyle", "msTransformStyle", "MozTransformStyle", "WebkitTransformStyle", "OTransformStyle"]);

            this.filterPrefix = getVendorPrefix(["WebkitFilter", "MozFilter", "OFilter", "msFilter", "filter"]);

            // we need additional check for IE detection
            if (this.filterPrefix == "filter") {
                var el = document.createElement('a');
                var prefixes = ["-ms-", "-webkit-", "-o-", "-moz-", ""];
                prefixes = prefixes.join('filter:blur(2px); ');
                el.style.cssText = prefixes;
                var result = (!!el.style.length && ((document.documentMode === undefined || document.documentMode > 9)));

                if (!result) {
                    this.filterPrefix = null;
                    ieHelper.help();

                }
            }

            //TODO:remove it - this is test in chrome
            //this.filterPrefix = null;
            //ieHelper.help();


            this.isSupportsPreserve3d = supportsPreserve3d()

        },

        createItem: function (w, h) {

            var item = new carousel.declarations.visualizers.CSS3Item();

            var c = document.createElement('div');
            c.style.width = w + "px";
            c.style.height = h + "px";
            c.classList.add('cItem');

            var preloadTxt = document.createElement('p');
            preloadTxt.innerText = "loading";
            preloadTxt.classList.add("preloadTxt");
            preloadTxt.style.width = w + "px";
            preloadTxt.style.lineHeight = h + "px";

            item.dom = c;
            item.preloadText = preloadTxt;

            item.preloadState();

            return item;
        },

        layout: function (items, immediate) {

            for (var i = 0; i < items.length; i++) {

                var item = items[i];

                if (immediate) {
                    item.dom.style[this.transitionPrefix] = "0s"
                } else {
                    item.dom.style[this.transitionPrefix] = "0.5s"
                }

                item.dom.style[this.transformPrefix] = this.positions[i];
                //if (!this.isSupportsPreserve3d) item.dom.style.zIndex = this.depths[i];
                item.dom.style.zIndex = this.depths[i];
                /* for IE10+ */

                item.position = i;
                item.updateEffects(i, this.filterPrefix);
            }
        },

        loadImage: function (item, link) {

            var img = document.createElement('img');
            item.addImage(img);

            var that = this;
            img.addEventListener('load', function () {
                that.onImageLoaded(item);
            }, false);

            img.src = link;
            item.addImage(img);
        },

        onImageLoaded: function (item) {
            item.loadedState();
        }
    }

    var CSS33VisualizerConstructor = new Function();
    decorate(CSS33VisualizerConstructor.prototype, CSS33Visualizer);
    declare("declarations.visualizers.CSS3Visualizer", CSS33VisualizerConstructor, carousel)

    //TODO: implement alternative/fallback visualizer implementations
    // Canvas visualizer
    // ...
    // Webgl visualizer
    // ...
    // preudo3d DOM visualizer
    // ...

    // Core component

    var defaultConfig, EngineTraits;


    //TODO: improve configuration object - provide more features and keys for
    defaultConfig = {
        numItems: 5,
        startIndex: 0,
        images: [
            {link: "content/500x350.ed1c24.ffffff.text=item+1.png"}
            , {link: "content/500x350.0072bc.ffffff.text=item+2.png"}
            , {link: "content/500x350.39b54a.ffffff.text=item+3.png"}
            , {link: "content/500x350.f26522.ffffff.text=item+4.png", props: {showBg: true}}
            , {link: "content/500x350.630460.ffffff.text=item+5.png"}
        ],
        itemSize: [500, 350],
        layoutDirection: "horizontal",
        transitionTime: 1
    }

    EngineTraits = {

        id: null,
        holder: null,
        cfg: defaultConfig,

        visuealizer: null,
        items: [],
        currentIndex: 2,


        setup: function (cfg) {

            var that = this;

            if (cfg) this.cfg = this.validateCfg(cfg);

            if (this.cfg.hasOwnProperty("visualizer") && (typeof cfg.visualizer == "string")) {
                switch (this.cfg.visualizer) {
                    case "canvas":
                    case "webgl":
                    case "canvas":
                    case "css3":
                        this.visualizer = new carousel.declarations.visualizers.CSS3Visualizer();
                    default:
                        this.visualizer = new carousel.declarations.visualizers.CSS3Visualizer();
                        break;
                }
            } else if (this.cfg.visualizer) {

                // we have external visualizer instance
                this.visualizer = this.cfg.visualizer;

            } else {
                //TODO: implement feature detection for automatic appropriate type selection;
                this.visualizer = new carousel.declarations.visualizers.CSS3Visualizer();
            }

            this.visualizer.invalidate = function () {
                that.invalidate();
            };

            this.visualizer.init();

            this.createDisplay();
            this.updateDisplay(true);
            this.loadImages();

            while (this.currentIndex != this.cfg.startIndex) {
                this.next(true);
            }

            this.updateDisplay(true);


            var onWheel = function  (e) {

                e = e || window.event;
                e.preventDefault ? e.preventDefault() : (e.returnValue = false);
                var delta = e.deltaY || e.detail || e.wheelDelta;

                that.onMouseWheel(delta);
            }

            if ('onwheel' in document) {
                // IE9+, FF17+, Ch31+
                this.holder.addEventListener("wheel", onWheel);
            } else if ('onmousewheel' in document) {
                // устаревший вариант события
                this.holder.addEventListener("mousewheel", onWheel);
            } else {
                // Firefox < 17
                this.holder.addEventListener("MozMousePixelScroll", onWheel);
            }
        },

        lastScrollTime:0,
        onMouseWheel : function (delta) {
            console.debug(delta);

            var now = new Date().getTime();

            if ((now - this.lastScrollTime) < 500) return;
            this.lastScrollTime = now;

            if (delta > 0)
                this.next();
            else
                this.prev();
        },

        validateCfg: function (cfg) {

            if (!cfg) cfg = {};

            for (var p in defaultConfig) {
                if (!cfg.hasOwnProperty(p)) cfg[p] = defaultConfig[p];
            }

            return cfg;
        },

        createDisplay: function () {

            var c, itemsContainer;

            if (!this.holder) {
                console.warn("Have no holder element for carousel. This is must be <div class='container'></div>. Empty element will be created");
                c = document.createElement('div');
                c.classList.add('container');
                document.body.appendChild(c);
            }

            c = document.createElement('div');
            c.classList.add('carousel');
            this.holder.appendChild(c);
            itemsContainer = c;

            this.items = [];

            for (var i = 0; i < this.cfg.numItems; i++) {
                c = this.createItem(i);
                if (c) {
                    itemsContainer.appendChild(c.dom);
                    this.items.push(c);
                }
            }
        },

        createItem: function (index) {

            var item = this.visualizer.createItem(this.cfg.itemSize[0], this.cfg.itemSize[1]);
            item.index = index;
            return item;
        },

        loadImages: function () {

            //TODO: Here we can improve code to achieve long image sequence with repeated renderers
            for (var i = 0; i < this.cfg.numItems; i++) {

                var item = this.items[i];
                if (!item) continue;

                var link = this.cfg.images[i]["link"]
                var props = this.cfg.images[i]["props"]

                if (props) {

                    for (var p in props) {
                        item[p] = props[p];
                    }
                }

                if (link) this.visualizer.loadImage(item, link);
            }
        },

        updateDisplay: function (immediate) {

            if (this.visualizer) this.visualizer.layout(this.items, immediate);

        },

        invalidate: function () {
            this.updateDisplay(true);
        },

        next: function (noredraw) {

            this.items.unshift(this.items.pop());
            this.currentIndex = this.items[2].index;

            if (!noredraw) this.updateDisplay();
        },

        prev: function (noredraw) {

            this.items.push(this.items.shift());
            this.currentIndex = this.items[2].index;

            if (!noredraw) this.updateDisplay();
        },

        toString: function () {
            return "[Engine instance]" + this.id;
        }

    }

    var engineConstructor = new Function();
    decorate(engineConstructor.prototype, EngineTraits);
    declare("declarations.Engine", engineConstructor, carousel);


    // export api
    carousel.global["carousel"] = carousel;

}());